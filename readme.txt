Curso "Crea una página web profesional con HTML CSS y Javascript" por Jordan Alexander
Duración: 2,5 horas

Herramientas:
* AOS: animate on scroll sirve para hacer más rápido animaciones en la página https://michalsnik.github.io/aos/

* Undraw.co: Un gran banco de ilustraciones SVG las cuales harán tu página web muy novedosa y profesional!

* Google fonts: Un banco de fuentes las cuales puedes integrar en tus páginas web con HTML y CSS

* FontAwesome: Un banco de iconos que puedes utilizar en tus proyectos el cual puede ser integrado en HTML.

* uiGradients: Un recurso que te permitirá agregar degradados a tu sitio web mediante CSS, lo cuál hará que se vea genial tu página web.

* Clippy: Un recurso el cual te permitirá crear elementos clip-path mediante un generador, el cuál lo hace más rápido y gráfico.
